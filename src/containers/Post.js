import React, { Component } from 'react';
import { func, shape, string } from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { get } from 'lodash';

import { getPostDetail } from '../actions';
import PostDetail from '../components/PostDetail';

const propTypes = {
  getPostDetail: func.isRequired,
  match: shape({
    params: shape({
      slug: string,
    }),
  }).isRequired,
};

class Post extends Component {
  constructor(props) {
    super(props);

    this.state = { loading: true };
  }

  componentDidMount() {
    const { getPostDetail, match } = this.props;
    const slug = get(match, 'params.slug', '');

    getPostDetail(slug).then(() => {
      this.setState({ loading: false });
    });
  }

  render() {
    const { post } = this.props;
    const { loading } = this.state;

    return (
      <div className="main-content">
        <div className="content-wrap">
          {loading ? <div className="content">Loading..</div> : <PostDetail post={post} />}
        </div>
      </div>
    );
  }
}

Post.propTypes = propTypes;

const mapStateToProps = ({ main }) => ({
  post: main.post,
});

const mapDispatchToProps = dispatch => bindActionCreators({ getPostDetail }, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Post);
