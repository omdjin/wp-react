import React, { Component } from 'react';
import { arrayOf, func, number, shape, string } from 'prop-types';
import { Link } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { getPosts } from '../actions';
import { getFeaturedMedia, stripTags } from '../constants/Helpers';

const propTypes = {
  getPosts: func.isRequired,
  posts: arrayOf(
    shape({
      id: number,
      slug: string,
    }),
  ),
};

const defaultProps = {
  posts: [],
};

class Home extends Component {
  componentDidMount() {
    this.props.getPosts();
  }

  renderArticles(post) {
    return (
      <article key={post.id} className="post-item">
        <header>
          <h2>
            <Link to={`/${post.slug}`}>{post.title.rendered}</Link>
          </h2>
          <div className="post-meta">
            {post.date} by {post._embedded.author[0].name}
          </div>
        </header>
        <div dangerouslySetInnerHTML={{ __html: stripTags(post.excerpt.rendered) }} className="block-content" />
        <div className="block-footer">
          <picture>
            <source
              media="(max-width: 300px)"
              srcSet={getFeaturedMedia(post._embedded['wp:featuredmedia'], 'thumbnail')}
            />
            <source
              media="(max-width: 735px)"
              srcSet={getFeaturedMedia(post._embedded['wp:featuredmedia'], 'medium')}
            />
            <source media="(min-width: 736px)" srcSet={getFeaturedMedia(post._embedded['wp:featuredmedia'])} />
            <img alt="" src={getFeaturedMedia(post._embedded['wp:featuredmedia'])} />
          </picture>
        </div>
      </article>
    );
  }

  render() {
    const { posts } = this.props;

    return (
      <div className="main-content">
        <div className="content-wrap">
          <div className="content">
            <div className="archive">{posts.map(this.renderArticles)}</div>
          </div>
        </div>
      </div>
    );
  }
}

Home.propTypes = propTypes;
Home.defaultProps = defaultProps;

function mapStateToProps({ main }) {
  return {
    posts: main.posts,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      getPosts,
    },
    dispatch,
  );
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Home);
