import React, { PureComponent } from 'react';

class About extends PureComponent {
  render() {
    return (
      <div className="main-content">
        <div className="content-wrap">
          <div className="content">
            <header className="App-header">
              <h1 className="App-title">ABOUT PAGE</h1>
            </header>
            <p className="App-intro">
              To get started, edit <code>src/App.js</code> and save to reload.
            </p>
          </div>
        </div>
      </div>
    );
  }
}

export default About;
