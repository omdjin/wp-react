import React, { Component, Fragment } from 'react';
import { arrayOf, func, number, shape, string } from 'prop-types';
import { Link } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { get } from 'lodash';
import { Helmet } from 'react-helmet';

import { getCategoryPost } from '../actions';
import { getFeaturedMedia, stripTags } from '../constants/Helpers';

const propTypes = {
  getCategoryPost: func.isRequired,
  posts: arrayOf(
    shape({
      id: number,
      slug: string,
    }),
  ),
};

const defaultProps = {
  posts: [],
};

class Category extends Component {
  componentDidMount() {
    const slug = get(this.props, 'match.params.slug', '');

    this.props.getCategoryPost(slug);
  }

  componentDidUpdate(prevProps) {
    const { getCategoryPost } = this.props;
    const prevSlug = get(prevProps, 'match.params.slug', '');
    const currentSlug = get(this.props, 'match.params.slug', '');

    if (prevSlug !== currentSlug) {
      getCategoryPost(currentSlug);
    }
  }

  renderArticles(post) {
    return (
      <article key={post.id} className="post-item">
        <header>
          <h2>
            <Link to={`/${post.slug}`}>{post.title.rendered}</Link>
          </h2>
          <div className="post-meta">
            {post.date} by {post._embedded.author[0].name}
          </div>
        </header>
        <div dangerouslySetInnerHTML={{ __html: stripTags(post.excerpt.rendered) }} className="block-content" />
        <div className="block-footer">
          <picture>
            <source
              media="(max-width: 300px)"
              srcSet={getFeaturedMedia(post._embedded['wp:featuredmedia'], 'thumbnail')}
            />
            <source
              media="(max-width: 735px)"
              srcSet={getFeaturedMedia(post._embedded['wp:featuredmedia'], 'medium')}
            />
            <source media="(min-width: 736px)" srcSet={getFeaturedMedia(post._embedded['wp:featuredmedia'])} />
            <img alt="" src={getFeaturedMedia(post._embedded['wp:featuredmedia'])} />
          </picture>
        </div>
      </article>
    );
  }

  render() {
    const { categories, match, posts } = this.props;

    if (!posts) {
      return <div>Loading..</div>;
    }

    const slug = get(match, 'params.slug', '');
    const category = categories.find(cat => cat.slug === slug);

    return (
      <div className="main-content">
        <div className="content-wrap">
          <div className="content">
            {category && (
              <Fragment>
                <Helmet>
                  <title>{category.name}</title>
                  <meta name="description" content={category.description} />
                </Helmet>
                <h1>{category.name}</h1>
              </Fragment>
            )}
            <div className="archive">{posts.map(this.renderArticles)}</div>
          </div>
        </div>
      </div>
    );
  }
}

Category.propTypes = propTypes;
Category.defaultProps = defaultProps;

function mapStateToProps({ main }) {
  return {
    categories: main.categories,
    posts: main.posts,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      getCategoryPost,
    },
    dispatch,
  );
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Category);
