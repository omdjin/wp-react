import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';
import Header from './components/Header';
import About from './containers/About';
import Category from './containers/Category';
import Home from './containers/Home';
import NotFound from './containers/NotFound';
import Post from './containers/Post';

import { appClass, containerClass } from './styles';

class App extends Component {
  render() {
    return (
      <div className={appClass}>
        <Header />
        <div className={containerClass}>
          <Switch>
            <Route exact path="/" component={Home} />
            <Route path="/about" component={About} />
            <Route path="/category/:slug" component={Category} />
            <Route path="/:slug" component={Post} />
            <Route component={NotFound} />
          </Switch>
        </div>
      </div>
    );
  }
}

export default App;
