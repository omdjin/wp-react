import { css, injectGlobal } from 'react-emotion';

injectGlobal`
body {
  margin: 0;
  padding: 0;
  font-family: medium-content-serif-font, Georgia, Cambria, 'Times New Roman', Times, serif;
  letter-spacing: 0;
  font-weight: 400;
  font-style: normal;
  font-size: 20px;
  line-height: 1.4;
  text-rendering: optimizeLegibility;
  color: rgba(0, 0, 0, 0.84);
}
`;

export const appClass = css({
  img: {
    maxWidth: '100%',
    height: 'auto',
  },
  a: {
    color: 'inherit',
    textDecoration: 'none',
  },
});

export const containerClass = css({
  width: '100%',
  maxWidth: '1032px !important',
  margin: '0 auto',
  '.main-content': {
    marginTop: 0,
    padding: '0 16px',
    '.content-wrap': {
      position: 'relative',
    },
    '.content': {
      padding: '0 8px',
    },
  },
});
