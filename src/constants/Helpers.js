export const getFeaturedMedia = (medias, size = null) => {
  if (medias && medias[0] && medias[0].source_url) {
    if (size) {
      if (size === 'thumbnail' && medias[0].media_details.sizes.thumbnail) {
        return medias[0].media_details.sizes.thumbnail.source_url;
      } else if (size === 'medium' && medias[0].media_details.sizes.medium) {
        return medias[0].media_details.sizes.medium.source_url;
      }
    }

    return medias[0].source_url;
  }

  return 'http://placehold.it/300x160';
};

export const stripTags = html => html.replace(/<(?:.|\n)*?>/gm, '');

export const cleanLinks = html => html.replace(/href="https:\/\/farizal.id|href="http:\/\/farizal.id/gi, 'href="');

export default stripTags;
