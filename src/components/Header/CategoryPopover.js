import React from 'react';
import { Link } from 'react-router-dom';

import { PopoverWrapper } from './styles';

const CategoryPopover = ({ categories, isMenuOpen }) => (
  <PopoverWrapper isOpen={isMenuOpen}>
    <div className="innerMenu">
      <ul role="menu">
        {categories.map(cat => (
          <li key={cat.id} className="list-item">
            <Link to={`/category/${cat.slug}`} className="button">
              {cat.name}
            </Link>
          </li>
        ))}
      </ul>
    </div>
    <div className="arrowUp" />
  </PopoverWrapper>
);

export default CategoryPopover;
