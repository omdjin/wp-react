import React, { Component } from 'react';
import { arrayOf, func, object, string } from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router-dom';
import { Helmet } from 'react-helmet';

import { getCategories, getInfo } from 'actions';
import { toggleMenu } from 'actions/actions';

import CategoryPopover from './CategoryPopover';

import { hamburger, headerClass, iconContainer, logoContainer, logoImage } from './styles';

class Header extends Component {
  static propTypes = {
    categories: arrayOf(object),
    description: string,
    getCategories: func.isRequired,
    getInfo: func.isRequired,
    name: string,
    onToggleMenu: func.isRequired,
  };

  static defaultProps = {
    categories: [],
    description: '',
    name: '',
  };

  componentDidMount() {
    this.props.getInfo();
    this.props.getCategories();
  }

  handleMenuClick = () => {
    const { isMenuOpen, onToggleMenu } = this.props;

    onToggleMenu(!isMenuOpen);
  };

  render() {
    const { categories, description, isMenuOpen, name } = this.props;

    return (
      <header className={headerClass}>
        <Helmet>
          <title>{`${name} - ${description}`}</title>
          <meta name="description" content={`${name} - ${description}`} />
        </Helmet>
        <div className={logoContainer}>
          <Link to="/">
            <img
              className={logoImage}
              src="https://cdn-images-1.medium.com/fit/c/96/96/0*SYFP39jdXatkRWhE."
              alt="logo"
            />
          </Link>
        </div>
        <div className={iconContainer}>
          <div className={hamburger} onClick={this.handleMenuClick}>
            <span />
            <span />
            <span />
          </div>
        </div>
        <CategoryPopover categories={categories} isMenuOpen={isMenuOpen} />
      </header>
    );
  }
}

function mapStateToProps({ action, main }) {
  return {
    name: main.name,
    description: main.description,
    categories: main.categories,
    isMenuOpen: action.isMenuOpen,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      getCategories,
      getInfo,
      onToggleMenu: toggleMenu,
    },
    dispatch,
  );
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Header);
