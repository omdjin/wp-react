import styled, { css } from 'react-emotion';

export const PopoverWrapper = styled('div')(
  {
    width: '230px',
    overflow: 'hidden',
    zIndex: 1,
    fontSize: '15px',
    display: 'none',
    padding: '15px',
    position: 'absolute',
    right: '0',
    top: '48px',
    '.innerMenu': {
      padding: '8px 0',
      overflow: 'auto',
      position: 'relative',
      maxWidth: '280px',
      borderRadius: '3px',
      background: '#fff',
      boxShadow: '0 1px 2px rgba(0,0,0,.25), 0 0 1px rgba(0,0,0,.35)',
      ul: {
        listStyle: 'none',
        padding: 0,
        margin: 0,
        '.list-item': {
          width: '100%',
          lineHeight: 1.4,
          whiteSpace: 'nowrap',
          'a.button': {
            display: 'inline-block',
            position: 'relative',
            background: 'rgba(0,0,0,0)',
            fontSize: '16px',
            textDecoration: 'none',
            cursor: 'pointer',
            userSelect: 'none',
            boxSizing: 'border-box',
            letterSpacing: 0,
            fontWeight: 400,
            fontStyle: 'normal',
            textRendering: 'optimizeLegibility',
            fontSmooth: 'antialiased',
            '-webkit-font-smoothing': 'antialiased',
            '-moz-osx-font-smoothing': 'grayscale',
            '-moz-font-feature-settings': `"liga" on`,
            padding: '7px 25px',
            textAlign: 'left',
            lineHeight: 1.4,
            color: 'rgba(0,0,0,.68)',
          },
        },
      },
    },
    '.arrowUp': {
      position: 'absolute',
      top: '1px',
      right: '22px',
      clip: 'rect(0 18px 14px -4px)',
      ':after': {
        content: `''`,
        display: 'block',
        width: '14px',
        height: '14px',
        background: '#fff',
        transform: 'rotate(45deg) translate(6px,6px)',
        boxShadow: '-1px -1px 1px -1px rgba(0,0,0,.54)',
      },
    },
  },
  ({ isOpen }) => isOpen && css({ display: 'block' }),
);

export const hamburger = css({
  width: '20px',
  position: 'relative',
  span: {
    display: 'block',
    width: '100%',
    height: '2px',
    marginTop: '4px',
    backgroundColor: 'rgba(0, 0, 0, 0.54)',
    borderRadius: '4px',
  },
  'span:first-child': {
    marginTop: '22px',
  },
});

export const headerClass = css({
  height: '56px',
  position: 'relative',
  maxWidth: '1032px !important',
  padding: '0 16px',
  margin: '0 auto',
  ':after': {
    clear: 'both',
  },
});

export const iconContainer = css({
  float: 'right',
});

export const logoContainer = css({
  float: 'left',
});

export const logoImage = css({
  width: '45px',
  height: '45px',
  marginTop: '6px',
  borderRadius: '50%',
});
