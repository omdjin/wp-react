import React from 'react';
import { Helmet } from 'react-helmet';

import { cleanLinks } from '../constants/Helpers';

const PostDetail = ({ post }) => {
  if (!post) {
    return <div>Loading...</div>;
  }

  return (
    <div className="content">
      <Helmet>
        <title>{`${post.title.rendered}`}</title>
        <meta name="description" content={post.excerpt.rendered} />
      </Helmet>
      <header className="App-header">
        <h1 className="App-title">{post.title.rendered}</h1>
      </header>
      <div dangerouslySetInnerHTML={{ __html: cleanLinks(post.content.rendered) }} className="post-content" />
    </div>
  );
};

export default PostDetail;
