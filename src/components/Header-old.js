import React, { Component } from 'react';
import { func } from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router-dom';
import { Helmet } from 'react-helmet';

import { getCategories, getInfo } from '../actions';
import '../styles/Header.css';
import logo from '../img/logo.svg';

const propTypes = {
  getCategories: func.isRequired,
  getInfo: func.isRequired,
};

class Header extends Component {
  componentDidMount() {
    this.props.getCategories();
    this.props.getInfo();
  }

  renderCategories(category) {
    return (
      <li key={category.id}>
        <Link to={`/category/${category.slug}`}>{category.name}</Link>
      </li>
    );
  }

  render() {
    const { categories, description, name } = this.props;

    return (
      <header className="App-navigation">
        <Helmet>
          <title>{`${name} - ${description}`}</title>
          <meta name="description" content={`${name} - ${description}`} />
        </Helmet>
        <div className="side-nav">
          <div className="side-nav-header">
            <Link to="/" title={`${name} ${description}`}>
              <img src={logo} className="App-logo" alt="logo" />
            </Link>
          </div>
          <div className="side-nav-body">
            <ul className="menu main-menu">
              {categories.map(this.renderCategories)}
              <li>
                <Link to="/about">About</Link>
              </li>
            </ul>
          </div>
        </div>
      </header>
    );
  }
}

Header.propTypes = propTypes;

function mapStateToProps({ main }) {
  return {
    name: main.name,
    description: main.description,
    categories: main.categories,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      getCategories,
      getInfo,
    },
    dispatch,
  );
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Header);
