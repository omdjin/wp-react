import axios from 'axios';

const { REACT_APP_BASE_API_URL, REACT_APP_API_URL } = process.env;

export const getInfo = () => axios.get(REACT_APP_BASE_API_URL);

export const getCategories = queryString => axios.get(`${REACT_APP_API_URL}categories?${queryString}`);

export const getPosts = queryString => axios.get(`${REACT_APP_API_URL}posts?_embed&${queryString}`);

export const getCategoryPost = slug =>
  axios.get(`${REACT_APP_API_URL}categories?_view&slug=${slug}`).then(({ data }) => {
    if (data.length > 0) {
      const catId = data[0].id;
      const queryString = `categories=${catId}`;

      return getPosts(queryString);
    }

    return [];
  });

export default {
  getCategories,
  getCategoryPost,
  getInfo,
  getPosts,
};
