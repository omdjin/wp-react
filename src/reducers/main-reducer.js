import {
  FETCH_CATEGORIES_SUCCESS,
  FETCH_INFO_SUCCESS,
  FETCH_POST_DETAIL_SUCCESS,
  FETCH_POSTS_SUCCESS,
} from '../constants';

import initialState from './initial-state';

export default function mainReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_CATEGORIES_SUCCESS:
      return {
        ...state,
        categories: action.categories,
      };
    case FETCH_INFO_SUCCESS:
      return {
        ...state,
        name: action.name,
        description: action.description,
      };
    case FETCH_POSTS_SUCCESS:
      return {
        ...state,
        posts: action.posts,
      };
    case FETCH_POST_DETAIL_SUCCESS:
      return {
        ...state,
        post: action.post,
      };
    default:
      return state;
  }
}
