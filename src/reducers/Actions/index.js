import { ACTION_TOGGLE_MENU } from '../../constants';

import initialState from './initial';

export default function actionReducer(state = initialState, action) {
  switch (action.type) {
    case ACTION_TOGGLE_MENU:
      return {
        ...state,
        isMenuOpen: action.isMenuOpen,
      };
    default:
      return state;
  }
}
