import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import actionReducer from './Actions';
import mainReducer from './main-reducer';

export default combineReducers({
  action: actionReducer,
  routing: routerReducer,
  main: mainReducer,
});
