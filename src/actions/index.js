import * as types from '../constants';
import API from '../api';

export const getCategories = (queryString = '') => dispatch =>
  API.getCategories(queryString).then(response => {
    dispatch({
      type: types.FETCH_CATEGORIES_SUCCESS,
      categories: response.data,
    });
  });

export const getInfo = () => dispatch =>
  API.getInfo().then(response => {
    dispatch({
      type: types.FETCH_INFO_SUCCESS,
      name: response.data.name,
      description: response.data.description,
    });
  });

export const getPosts = (queryString = '') => dispatch =>
  API.getPosts(queryString).then(response => {
    dispatch({
      type: types.FETCH_POSTS_SUCCESS,
      posts: response.data,
    });
  });

export const getPostDetail = slug => dispatch =>
  API.getPosts(`slug=${slug}&`).then(response => {
    if (response.data[0]) {
      dispatch({
        type: types.FETCH_POST_DETAIL_SUCCESS,
        post: response.data[0],
      });
    }
  });

export const getCategoryPost = slug => dispatch =>
  API.getCategoryPost(slug).then(response => {
    dispatch({
      type: types.FETCH_POSTS_SUCCESS,
      posts: response.data,
    });
  });
