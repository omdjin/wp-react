import { ACTION_TOGGLE_MENU } from '../constants';

export const toggleMenu = isMenuOpen => dispatch => {
  dispatch({
    type: ACTION_TOGGLE_MENU,
    isMenuOpen,
  });
};
